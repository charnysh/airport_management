# README

## Frontend
 - SPA, React

 - Дизайн на усмотрение студента
 
 - Поддерживаем две последние версии браузера, т.е. можно использовать последние фишки
 
 - Для реализации только часть администрирования

## Логин/Регистрация пользователя
 
 - Страницы портала доступны только авторизованным пользователям

## Портал Администрирования
 - Доступна только под админским аккаунтом
 
 - Добавление информации об аэропортах
 
 - Добавление информации о типах самолетов, включая количество мест и схема расположения мест в самолете, места могут иметь разный класс (бизнес, эконом класс и так далее, тоже настраивается в админке), максимальная грузоподъемность
 
 - Добавление информации о рейсе (аэропорт отправки, аэропорт назначения, время, тип самолета, цена билета с привязкой к типу места), лимит бесплатного багажа по весу, цена за каждые N кг перегруза.
 
 - Подсказки при наборах аэропорта
 
 - Валидация форм

## Клиентский портал
 - Возможность найти рейс по аэропорту / городу отправления и назначения на нужную дату, возможность подобрать рейсы на период туда и обратно, возможность указать количество требуемых билетов 1 или более

 - Подсказки при наборах аэропорта / города из существующих в базе

 - Возможность забронировать место/места, сначала выбираешь рейс, затем выбираешь места в самолете, они блокируются на 15 минут (например) (не доступно для выбора другим пользователям), затем пользователь указывает будет ли багаж и в каком количестве, далее страница с расчетом стоимости и возможности добавить такое количество багажа и кнопкой подтвердить. после этого считается что места забронированы и больше не доступны для выбора
 
 - Возможность просмотреть список своих заказов с фильтрами по будущим и прошлым (по умолчанию только будущие)

## Backend
 - .Net Core

 - Authorization and authentication (JWT or Cookies, admin and user roles)

 - NoSQL or SQL database
REST like API
Server side validation of client data



## PRAVKI

1) consider creating mapping profiles for each type of entity/viewmodel

2) consider using reflection to initiate dependency injector

(+) 3) [Authorize] can be applied to the whole class (BaseController)

(+) 4) it's better to use next form of dependency injection registration:
services.AddScoped<IRepository<Airport>, AirportsRepository>();

than

services.AddScoped<IRepository<Airport>>(provider => new AirportsRepository(provider.GetService<RepositoryContext>()));

(+) 5) are StaticFiles and CookiePolicy required?

(+) 6) using injected entities

public AuthorizationController(IUsersService service, IMapper mapper)

{

this.service = service as UsersService;

this.mapper = mapper;

}

casting to specific type makes interface useless

7) token generating is related to server part of web app itself not to app business logic (authentication is not related to business logic)

(+) 8) it makes sense to have specific interfaces for repositories for each domain entity