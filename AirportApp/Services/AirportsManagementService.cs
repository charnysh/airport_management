﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Database.Repositories;
using Database.Interfaces;
using Services.Interfaces;

namespace Services
{
    public class AirportsManagementService : IAirportsManagementService
    {
        private IAirportsRepository _airportsRepository;
        public AirportsManagementService(IAirportsRepository airportsRepository)
        {
            _airportsRepository = airportsRepository;
        }

        public AirportsManagementService()
        {
        }

        public void AddItem(Airport airport)
        {
            _airportsRepository.Create(airport);
        }

        public Airport GetItem(int id)
        {
            return _airportsRepository.GetItem(id);
        }

        public List<Airport> GetItems()
        {
            return _airportsRepository.GetItems();
        }

        public void RemoveItem(int id)
        {
            _airportsRepository.Delete(id);
        }

        public List<Flight> GetAirportArrivingFlights(int airportID)
        {
            return _airportsRepository.GetItem(airportID).ArrivingFlights;
        }

        public List<Flight> GetAirportDepartureFlights(int airportID)
        {
            return _airportsRepository.GetItem(airportID).DepartureFlights;
        }

        public void UpdateItem(Airport item)
        {
            _airportsRepository.Update(item);
        }

        public List<Airport> GetAiportCityNameSuggestions(string input)
        {
            return _airportsRepository.AiportCityNameSuggestions(input);
        }
    }
}
