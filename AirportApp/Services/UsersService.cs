﻿using System.Collections.Generic;
using Models;
using Services.Interfaces;
using Database.Interfaces;
using Utilities.ErrorHanding.Exceptions;


namespace Services
{
    public class UsersService : IUsersService
    {
        private IUsersRepository _usersRepository;
        private ICredentialsRepository _credentialsRepository;

        public UsersService(IUsersRepository usersRepository, ICredentialsRepository credentialsRepository)
        {
            _usersRepository = usersRepository;
            _credentialsRepository = credentialsRepository;
        }

        public void AddUser(User user)
        {
            if (!ValidateCredential(user.Credential))
                throw new LoginAlreadyUsedException();
            
            _usersRepository.Create(user);
        }

        public bool ValidateCredential(Credential credential)
        {
            if (_credentialsRepository.GetCredentialByLogin(credential.Login) != null)
                return false;
            if (credential.Password == credential.Login)
                return false;
            if (credential.Password.Length < 5)
                return false;
            return true;
        }

        public Credential GetCredentialByLogin(string login)
        {
            return _credentialsRepository.GetCredentialByLogin(login);
        }

        public User GetUserByCredentialID(int id)
        {
            return _usersRepository.GetUserByCredentialID(id);
        }

        public List<User> GetUsers(string role = null)
        {
            if (role != null)
                return _usersRepository.GetItems().FindAll(u => u.Role == role);
            return _usersRepository.GetItems();
        }
    }
}
