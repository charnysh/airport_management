﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface ISeatsManagementService
    {
        void CreateReservation(Reservation reservation, string userLogin);

        void CancelReservation(int reservationId);

        Ticket CreateTicket(int reservationId);

        List<Seat> GetFlightSeats(int flightId);

        List<Ticket> GetUserTickets(string usersLogin);

        List<Ticket> GetTickets();

        List<Reservation> GetReservations();
    }
}
