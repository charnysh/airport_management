﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IUsersService
    {
        void AddUser(User user);

        bool ValidateCredential(Credential credential);

        Credential GetCredentialByLogin(string login);

        User GetUserByCredentialID(int id);

        List<User> GetUsers(string role = null);
    }
}
