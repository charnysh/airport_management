﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IAirportsManagementService : IManagementService<Airport>
    {
        List<Airport> GetAiportCityNameSuggestions(string input);
    }
}
