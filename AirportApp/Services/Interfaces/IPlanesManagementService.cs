﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IPlanesManagementService : IManagementService<Plane>
    {
        List<Seat> GetPlaneSeats(int planeId);
        void AddSeats(int planeId, List<Seat> seats);
    }
}
