﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IFlightsManagementService : IManagementService<Flight>
    {
        List<Flight> FindFlightsByDestinationAirportID(int airportID);

        List<Flight> FindFlightsByDepartureAirportID(int airportID);

        List<Flight> GetFlightsByDepartureAirportCode(string code);

        List<Flight> GetFlightsByDepartureAirportCity(string city);

        List<Flight> GetFlightsByDestinationAirportCode(string code);

        List<Flight> GetFlightsByDestinationAirportCity(string city);

        List<Flight> GetFlightsByLeavingDate(DateTime dateTime);

        List<Flight> GetFlightsByArrivingDate(DateTime dateTime);

        List<Seat> GetSeats(int flightId);
    }
}
