﻿using System;
using Models;
using Database.Repositories;
using System.Collections.Generic;
using System.Linq;
using Services.Interfaces;
using Database.Interfaces;

namespace Services
{
    public class PlanesManagementService : IPlanesManagementService
    {
        private IPlanesRepository _planesRepository;
        private ISeatsRepository _seatsRepository;

        public PlanesManagementService(IPlanesRepository planesRepository, ISeatsRepository seatsRepository)
        {
            _planesRepository = planesRepository;
            _seatsRepository = seatsRepository;
        }

        public void AddItem(Plane plane)
        {
            _planesRepository.Create(plane);
        }

        public List<Plane> GetItems()
        {
            return _planesRepository.GetItems();
        }

        public Plane GetItem(int id)
        {
            return _planesRepository.GetItem(id);
        }

        public List<Seat> GetPlaneSeats(int planeId)
        {
            return _seatsRepository.GetSeatsByPlaneId(planeId);
        }

        public void AddSeats(int planeId, List<Seat> seats)
        {
            foreach (var seat in seats)
            {
                seat.PlaneID = planeId;
                _seatsRepository.Create(seat);
            }
        }

        public void RemoveItem(int id)
        {
            _planesRepository.Delete(id);
        }

        
        
    }
}
