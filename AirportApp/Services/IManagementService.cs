﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public interface IManagementService<T> where T: class
    {
        void AddItem(T item);
        T GetItem(int id);
        List<T> GetItems();
        void RemoveItem(int id);

        
    }
}
