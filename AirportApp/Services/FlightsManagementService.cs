﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Models;
using Database.Repositories;
using Database.Interfaces;
using Services.Interfaces;

namespace Services
{
    public class FlightsManagementService : IFlightsManagementService
    {
        private IFlightsRepository _flightsRepository;
        private IAirportsRepository _airportsRepository;
        private ISeatsRepository _seatsRepository;

        public FlightsManagementService(IFlightsRepository flightsRepository, IAirportsRepository airportsRepository, 
            ISeatsRepository seatsRepository)
        {
            _flightsRepository = flightsRepository;
            _airportsRepository = airportsRepository;
            _seatsRepository = seatsRepository;
        }

        public void AddItem(Flight flight)
        {
            _flightsRepository.Create(flight);
        }

        public Flight GetItem(int id)
        {
            var flight = _flightsRepository.GetItem(id);
            return flight;
        }
        
        public List<Flight> GetItems()
        {
            return _flightsRepository.GetItems();
        }

        public void RemoveItem(int id)
        {
            _flightsRepository.Delete(id);
        }

        public List<Flight> FindFlightsByDestinationAirportID(int airportID)
        {
            return _flightsRepository.FindFlightsByDestinationAirportID(airportID);
        }

        public List<Flight> FindFlightsByDepartureAirportID(int airportID)
        {
            return _flightsRepository.GetItems().FindAll(f => f.FromAirportID == airportID);
        }

        public void UpdateItem(Flight item)
        {
            _flightsRepository.Update(item);
        }

        public List<Flight> GetFlightsByDepartureAirportCode(string code)
        {
            var airport = _airportsRepository.GetAirportByCode(code);// check if airport exists or chek it inside repo get method
            return _flightsRepository.FindFlightsByDepartureAirportID(airport.ID);
        }

        public List<Flight> GetFlightsByDepartureAirportCity(string city)
        {
            var airports = _airportsRepository.AiportCityNameSuggestions(city);
            List<Flight> results = new List<Flight>();
            foreach (var airport in airports)
            {
                results.AddRange(_flightsRepository.FindFlightsByDepartureAirportID(airport.ID));
            }
            return results;
        }

        public List<Flight> GetFlightsByDestinationAirportCode(string code)
        {
            var airport = _airportsRepository.GetAirportByCode(code);
            return _flightsRepository.FindFlightsByDestinationAirportID(airport.ID);
        }

        public List<Flight> GetFlightsByDestinationAirportCity(string city)
        {
            var airports = _airportsRepository.AiportCityNameSuggestions(city);
            List<Flight> results = new List<Flight>();
            foreach (var airport in airports)
            {
                results.AddRange(_flightsRepository.FindFlightsByDestinationAirportID(airport.ID));
            }
            return results;
        }

        public List<Flight> GetFlightsByLeavingDate(DateTime dateTime)
        {
            return _flightsRepository.GetFlightsByLeavingDate(dateTime);
        }

        public List<Flight> GetFlightsByArrivingDate(DateTime dateTime)
        {
            return _flightsRepository.GetFlightsByArrivingDate(dateTime);
        }

        public List<Seat> GetSeats(int flightId)
        {
            return _seatsRepository.GetSeatsByFlightId(flightId);
        }
    }
}
