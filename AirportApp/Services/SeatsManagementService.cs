﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Services.Interfaces;
using Database.Interfaces;
using System.Linq;

namespace Services
{
    public class SeatsManagementService : ISeatsManagementService
    {
        protected IReservationsRepository _reservationRepository;
        protected ISeatsRepository _seatsRepository;
        protected ITicketsRepository _ticketsRepository;
        protected IUsersRepository _usersRepository;


        public SeatsManagementService(IReservationsRepository reservationRepository,
            ITicketsRepository ticketsRepository,
            ISeatsRepository seatsRepository,
            IUsersRepository usersRepository)
        {
            _reservationRepository = reservationRepository;
            _seatsRepository = seatsRepository;
            _ticketsRepository = ticketsRepository;
            _usersRepository = usersRepository;
        }

        public void CreateReservation(Reservation reservation, string userLogin)
        {
            var userId = _usersRepository.GetUserByLogin(userLogin).ID;
            reservation.UserId = userId;
            _reservationRepository.Create(reservation);
        }

        public void CancelReservation(int reservationId)
        {
            _reservationRepository.Delete(reservationId);
        }

        public List<Seat> GetFlightSeats(int flightId)
        {
            return _seatsRepository.GetSeatsByFlightId(flightId);
        }

        public List<Seat> GetReservedSeatsByFlightId(int flightId)
        {
            return _seatsRepository.GetFlightReservedSeats(flightId);
        }

        public Ticket CreateTicket(int reservationId)
        {
            var reservation = _reservationRepository.GetItem(reservationId);
            var ticket = new Ticket
            {
                SeatId = reservation.SeatId,
                UserId = reservation.UserId,
                FlightId = reservation.FlightId,
                DateBought = reservation.ReservationDate
            };
            _ticketsRepository.Create(ticket);
            _reservationRepository.Delete(reservationId);
            return ticket;
        }

        public List<Ticket> GetUserTickets(string usersLogin)
        {
            return _ticketsRepository.GetUserTickets(usersLogin);
        }

        public List<Reservation> GetReservations()
        {
            return _reservationRepository.GetItems();
        }

        public List<Ticket> GetTickets()
        {
            return _ticketsRepository.GetItems();
        }

    }
}
