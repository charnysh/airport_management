﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using ErrorHanding.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace ErrorHanding
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(httpContext, exception);
            }
        }

        private static Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            string result = "";
            if (exception is IdNotFoundException)
            {
                code = HttpStatusCode.NotFound;
                result = JsonConvert.SerializeObject(new { error = exception.GetType().ToString(), message = "Id not found", id = exception.Message });
            }
            else if (exception is CredentialIncorrectException)
            {
                code = HttpStatusCode.Unauthorized;
                result = JsonConvert.SerializeObject(new { error = exception.GetType().ToString(), message = "credentials incorrect" });
            }
            else if (exception is DbUpdateException)
            {
                code = HttpStatusCode.UnprocessableEntity;
                result = JsonConvert.SerializeObject(new { error = exception.GetType().ToString(), message = "cannot update DB" });
            }
            else
            {
                result = JsonConvert.SerializeObject(new { error = exception.GetType().ToString(), message = exception.Message });
            }

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)code;
            return httpContext.Response.WriteAsync(result);
        }
    }
}
