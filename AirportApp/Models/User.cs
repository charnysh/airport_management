﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class User
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Role { get; set; }
        public int CredentialID { get; set; }
        public virtual Credential Credential { get; set; }
        public override string ToString() => $"{Name} {Role}";
    }
}
