﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Seat
    {
        public enum SeatPosition
        {
            LeftWindow, LeftMiddle, LeftAside, RightWindow, RightMiddle, RightAside
        }

        public int ID { get; set; }
        public string Class { get; set; }
        public SeatPosition Position { get; set; }

        [Required]
        public int Number { get; set; }
       
        public int PlaneID { get; set; }
        public virtual Plane Plane { get; set; }

        public virtual List<Reservation> Reservations { get; set; }

        public virtual List<Ticket> Tickets { get; set; }

        public override string ToString() => $"{ID} {Class} position: {Position} #{Number}";
    }
}
 