﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Models.Interfaces;

namespace Models
{
    public class Airport : IModel
    {
        public int ID { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Code { get; set; }
        public virtual List<Flight> DepartureFlights { get; set; }
        public virtual List<Flight> ArrivingFlights { get; set; }
        public override string ToString() => $"{ID} {City} {Code}";

        public void CopyData(Airport other)
        {
            this.City = other.City;
            this.Code = other.Code;
            this.DepartureFlights = other.DepartureFlights;
            this.ArrivingFlights = other.ArrivingFlights;
        }
    }
}
