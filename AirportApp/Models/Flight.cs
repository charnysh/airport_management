﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class Flight
    {
        [Key]
        public int ID { get; set; }

        public int FromAirportID { get; set; }

        public virtual Airport FromAirport { get; set; }

        public int ToAirportID { get; set; }

        public virtual Airport ToAirport { get; set; }

        public DateTime LeaveDate { get; set; }

        public DateTime ArriveDate { get; set; }

        public int PlaneID { get; set; }

        public virtual Plane Plane { get; set; }

        public virtual List<Reservation> Reservations { get; set; }

        public virtual List<Ticket> Tickets { get; set; }


        [Column(TypeName = "Money")]
        public Decimal ExcessBaggageCharge {get; set;}

        public int FreeBaggageWeight { get; set; }
    }
}
