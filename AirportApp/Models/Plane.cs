﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Plane
    {
        public int ID { get; set; }
        [Required]
        public string Model { get; set; }
        public virtual List<Flight> Flights { get; set; }
        public virtual List<Seat> Seats { get; set; }

        [Required]
        public int LoadCapacity { get; set; }
        public override string ToString() => $"{ID} {Model}";
    }
}
