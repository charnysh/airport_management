﻿using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Ticket: IModel
    {
        public int ID { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public int SeatId { get; set; }

        public virtual Seat Seat { get; set; }

        public int FlightId { get; set; }

        public virtual Flight Flight { get; set; }

        public DateTime DateBought { get; set; }
    }
}
