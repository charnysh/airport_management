﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Passenger
    {
        public int ID { get; set; }
        public string Name { get; set; }
      
        public int SeatID { get; set; }
        public virtual Seat Seat { get; set; }
        
        public override string ToString()
        {
            return $"{ID} {Name} ";
        }
    }
}
