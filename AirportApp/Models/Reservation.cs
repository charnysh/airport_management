﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Reservation
    {
        public int ID { get; set; }

        public int SeatId { get; set; }

        public virtual Seat Seat { get; set; }

        public int FlightId { get; set; }

        public virtual Flight Flight { get; set; }

        public int UserId { get; set; }

        public DateTime ReservationDate { get; set; }
    }
}
