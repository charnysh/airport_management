﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Credential 
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(30)]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        public virtual User User { get; set; }

        public bool Match(Credential other)
        {
            if (Login != other.Login || Password != other.Password)
                return false;
            return true;
        }
    }
}
