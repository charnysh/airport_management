﻿using Database;
using Database.Interfaces;
using Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Services.Interfaces;
using System;

namespace Composition
{
    public class CompositionRoot
    {
        //static
        IConfiguration _configuration;
        public CompositionRoot(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void ComposeRoot(IServiceCollection services)
        {
            //rename connection
            services.AddDbContext<DatabaseContext>(options => options.UseLazyLoadingProxies()
                                                                     .UseSqlServer(_configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IAirportsRepository, AirportsRepository>();

            services.AddScoped<IFlightsRepository, FlightsRepository>();

            services.AddScoped<IPlanesRepository, PlanesRepository>();

            services.AddScoped<ISeatsRepository, SeatsRepository>();

            services.AddScoped<IUsersRepository, UsersRepository>();
         

            services.AddScoped<ICredentialsRepository, CredentialsRepository>();

            services.AddScoped<IUsersService, UsersService>();

            services.AddScoped<IAirportsManagementService, AirportsManagementService>();

            services.AddScoped<IFlightsManagementService, FlightsManagementService>();

            services.AddScoped<IPlanesManagementService, PlanesManagementService>();
        }
    }
}
