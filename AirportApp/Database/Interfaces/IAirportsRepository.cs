﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface IAirportsRepository: IRepository<Airport>
    {
        List<Airport> AiportCityNameSuggestions(string input);
        Airport GetAirportByCode(string code);
    }
}
