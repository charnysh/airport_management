﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface ISeatsRepository : IRepository<Seat>
    {
        List<Seat> GetSeatsByPlaneId(int planeId);

        List<Seat> GetSeatsByFlightId(int flightId);

        List<Seat> GetFlightReservedSeats(int flightId);
    }
}
