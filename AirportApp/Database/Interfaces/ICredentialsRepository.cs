﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface ICredentialsRepository : IRepository<Credential>
    {
        Credential GetCredentialByLogin(string login);
    }
}
