﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface IReservationsRepository : IRepository<Reservation>
    {
        void ReserveSeat(int seatId, int flightId, int userId);
        void ClearReservation(Func<Reservation, bool> predicate);

        List<Reservation> GetReservationsByFlight(int flightId);
    }
}
