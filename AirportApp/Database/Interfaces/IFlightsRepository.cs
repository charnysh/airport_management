﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface IFlightsRepository : IRepository<Flight>
    {
        List<Flight> FindFlightsByDestinationAirportID(int airportID);
        List<Flight> FindFlightsByDepartureAirportID(int airportID);

        List<Flight> GetFlightsByLeavingDate(DateTime dateTime);

        List<Flight> GetFlightsByArrivingDate(DateTime dateTime);
    }
}
