﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface IUsersRepository : IRepository<User>
    {
        User GetUserByCredentialID(int credentialID);

        User GetUserByLogin(string login);
    }
}
