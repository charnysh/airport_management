﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Interfaces
{
    public interface IRepository<T> where T: class
    {
        void Create(T item);

        void Update(T item);

        void Delete(int id);

        T GetItem(int id);

        List<T> GetItems();
    }
}
