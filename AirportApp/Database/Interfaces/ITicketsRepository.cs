﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Database.Interfaces
{
    public interface ITicketsRepository : IRepository<Ticket>
    {
        List<Ticket> GetUserTickets(int userId);
        List<Ticket> GetUserTickets(string userLogin);
    }
}
