﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class reservation_n_ticket_connections_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Flights_FlightID",
                table: "Ticket");

            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Seats_SeatID",
                table: "Ticket");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ticket",
                table: "Ticket");

            migrationBuilder.RenameTable(
                name: "Ticket",
                newName: "Tickets");

            migrationBuilder.RenameIndex(
                name: "IX_Ticket_SeatID",
                table: "Tickets",
                newName: "IX_Tickets_SeatID");

            migrationBuilder.RenameIndex(
                name: "IX_Ticket_FlightID",
                table: "Tickets",
                newName: "IX_Tickets_FlightID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Flights_FlightID",
                table: "Tickets",
                column: "FlightID",
                principalTable: "Flights",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Seats_SeatID",
                table: "Tickets",
                column: "SeatID",
                principalTable: "Seats",
                principalColumn: "SeatID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Flights_FlightID",
                table: "Tickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Seats_SeatID",
                table: "Tickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets");

            migrationBuilder.RenameTable(
                name: "Tickets",
                newName: "Ticket");

            migrationBuilder.RenameIndex(
                name: "IX_Tickets_SeatID",
                table: "Ticket",
                newName: "IX_Ticket_SeatID");

            migrationBuilder.RenameIndex(
                name: "IX_Tickets_FlightID",
                table: "Ticket",
                newName: "IX_Ticket_FlightID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ticket",
                table: "Ticket",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Flights_FlightID",
                table: "Ticket",
                column: "FlightID",
                principalTable: "Flights",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Seats_SeatID",
                table: "Ticket",
                column: "SeatID",
                principalTable: "Seats",
                principalColumn: "SeatID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
