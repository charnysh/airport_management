﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class ID_fields_renamed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SeatID",
                table: "Seats",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "PassengerID",
                table: "Passengers",
                newName: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Seats",
                newName: "SeatID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Passengers",
                newName: "PassengerID");
        }
    }
}
