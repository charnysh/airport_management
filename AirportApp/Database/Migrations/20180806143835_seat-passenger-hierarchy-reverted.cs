﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class seatpassengerhierarchyreverted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats");

            migrationBuilder.DropIndex(
                name: "IX_Seats_PassengerID",
                table: "Seats");

            migrationBuilder.DropColumn(
                name: "PassengerID",
                table: "Seats");

            migrationBuilder.AddColumn<int>(
                name: "SeatID",
                table: "Passengers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Passengers_SeatID",
                table: "Passengers",
                column: "SeatID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Passengers_Seats_SeatID",
                table: "Passengers",
                column: "SeatID",
                principalTable: "Seats",
                principalColumn: "SeatID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Passengers_Seats_SeatID",
                table: "Passengers");

            migrationBuilder.DropIndex(
                name: "IX_Passengers_SeatID",
                table: "Passengers");

            migrationBuilder.DropColumn(
                name: "SeatID",
                table: "Passengers");

            migrationBuilder.AddColumn<int>(
                name: "PassengerID",
                table: "Seats",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Seats_PassengerID",
                table: "Seats",
                column: "PassengerID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats",
                column: "PassengerID",
                principalTable: "Passengers",
                principalColumn: "PassengerID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
