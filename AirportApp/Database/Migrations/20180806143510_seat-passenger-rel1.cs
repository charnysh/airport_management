﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class seatpassengerrel1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Seats_Planes_PlaneID",
                table: "Seats");

            migrationBuilder.AddForeignKey(
                name: "FK_Seats_Planes_PlaneID",
                table: "Seats",
                column: "PlaneID",
                principalTable: "Planes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Seats_Planes_PlaneID",
                table: "Seats");

            migrationBuilder.AddForeignKey(
                name: "FK_Seats_Planes_PlaneID",
                table: "Seats",
                column: "PlaneID",
                principalTable: "Planes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
