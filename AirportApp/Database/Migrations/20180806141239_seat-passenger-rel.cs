﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class seatpassengerrel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats");

            
            

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });

           

            migrationBuilder.AddForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats",
                column: "PassengerID",
                principalTable: "Passengers",
                principalColumn: "PassengerID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Flights_Airports_FromAirportID",
                table: "Flights");

            migrationBuilder.DropForeignKey(
                name: "FK_Flights_Airports_ToAirportID",
                table: "Flights");

            migrationBuilder.DropForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Airports",
                table: "Airports");

            migrationBuilder.RenameTable(
                name: "Airports",
                newName: "Airport");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Airport",
                table: "Airport",
                column: "AirportID");

            migrationBuilder.AddForeignKey(
                name: "FK_Flights_Airport_FromAirportID",
                table: "Flights",
                column: "FromAirportID",
                principalTable: "Airport",
                principalColumn: "AirportID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Flights_Airport_ToAirportID",
                table: "Flights",
                column: "ToAirportID",
                principalTable: "Airport",
                principalColumn: "AirportID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Seats_Passengers_PassengerID",
                table: "Seats",
                column: "PassengerID",
                principalTable: "Passengers",
                principalColumn: "PassengerID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
