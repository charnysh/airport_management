﻿using Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Database
{
    public class DatabaseContext : DbContext
    {
        
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<Plane> Planes { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flight>()
                .HasOne(f => f.Plane)
                .WithMany(p => p.Flights);

            modelBuilder.Entity<Reservation>()
                .HasOne(r => r.Flight)
                .WithMany(f => f.Reservations)
                .HasForeignKey(r => r.FlightId);

            modelBuilder.Entity<Reservation>()
                .HasOne(r => r.Seat)
                .WithMany(s => s.Reservations)
                .HasForeignKey(r => r.SeatId);

            modelBuilder.Entity<Airport>()
                .HasMany(a => a.ArrivingFlights)
                .WithOne(f => f.ToAirport)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Airport>()
                .HasMany(a => a.DepartureFlights)
                .WithOne(f => f.FromAirport)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Plane>()
                .HasMany(p => p.Seats)
                .WithOne(s => s.Plane)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Credential>()
                .HasOne(s => s.User)
                .WithOne(u => u.Credential);
            
        }
    }
}
