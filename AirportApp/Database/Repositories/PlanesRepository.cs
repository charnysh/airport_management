﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Database.Interfaces;


namespace Database.Repositories
{
    public class PlanesRepository : BaseRepository<Plane>, IPlanesRepository
    {
        public PlanesRepository(DatabaseContext context)
            : base(context)
        { }
    }
}
