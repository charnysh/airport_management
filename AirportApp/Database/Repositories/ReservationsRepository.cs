﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Database.Interfaces;
using Models;

namespace Database.Repositories
{
    public class ReservationsRepository : BaseRepository<Reservation>, IReservationsRepository
    {
        public ReservationsRepository(DatabaseContext context) : base(context)
        {
        }

        public void ClearReservation()
        {
            var overdue = Context.Reservations.Where(r => r.ReservationDate - DateTime.Now > TimeSpan.FromMinutes(15)).ToList();
            Context.Reservations.RemoveRange(overdue);
        }

        public void ClearReservation(Func<Reservation, bool> predicate)
        { }

        public override void Create(Reservation reservation)
        {
            reservation.ReservationDate = DateTime.Now;
            base.Create(reservation);
        }

        public void ReserveSeat(int seatId, int flightId)
        {
            Create(new Reservation { SeatId = seatId, FlightId = flightId, ReservationDate = DateTime.Now });
        }

        public void ReserveSeat(int seatId, int flightId, int userId)
        {
            throw new NotImplementedException();
        }

        public List<Reservation> GetReservationsByFlight(int flightId)
        {
            return Context.Reservations.Where(r => r.FlightId == flightId).ToList();
        }

        
    }
}
