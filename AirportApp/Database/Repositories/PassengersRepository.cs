﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Database.Interfaces;

namespace Database.Repositories
{
    public class PassengersRepository : BaseRepository<Passenger>, IPassengersRepository
    {
        public PassengersRepository(DatabaseContext context)
            : base(context)
        {  }
    }
}
