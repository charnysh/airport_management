﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Database.Interfaces;

namespace Database.Repositories
{
    public class UsersRepository : BaseRepository<User>, IUsersRepository
    {
        public UsersRepository(DatabaseContext context)
            : base(context)
        { }


        public User GetUserByCredentialID(int credentialID)
        {
            var item = Context.Users.Single(u => u.CredentialID == credentialID);
            return item;
        }

        public User GetUserByLogin(string login)
        {
            return Context.Users.Single(u => u.Credential.Login == login);
        }
    }
}
