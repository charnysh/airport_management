﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Database.Interfaces;
using System.Linq;

namespace Database.Repositories
{
    public class TicketsRepository : BaseRepository<Ticket>, ITicketsRepository
    {
        public TicketsRepository(DatabaseContext context) : base(context)
        {
        }

        public List<Ticket> GetUserTickets(int userId)
        {
            return Context.Tickets.Where(t => t.UserId == userId).ToList();
        }

        public List<Ticket> GetUserTickets(string login)
        {
            return Context.Tickets.Where(t => t.User.Credential.Login == login).ToList();
        }
    }
}
