﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Models;
using Database.Interfaces;

namespace Database.Repositories
{
    public class CredentialsRepository : BaseRepository<Credential>, ICredentialsRepository
    {
        public CredentialsRepository(DatabaseContext context) : base(context)
        { }

        public Credential GetCredentialByLogin(string login)
        {
            var item = GetItems().Where(c => c.Login == login).SingleOrDefault();
            return item;
        }
    }
}
