﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Database.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        protected DatabaseContext Context;
        protected DbSet<T> DbSet;

        public BaseRepository(DatabaseContext context)
        {
            this.Context = context;
            DbSet = context.Set<T>();
        }
        public virtual void Create(T item)
        {
            DbSet.Add(item);
            Context.SaveChanges();
        }

        public virtual void Update(T item)
        {
            Context.Entry(item).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            T item = DbSet.Find(id);
            if (item != null)
                DbSet.Remove(item);
            Context.SaveChanges();
        }

        
        public virtual T GetItem(int id)
        {
            return DbSet.Find(id);
        }
        
        public List<T> GetItems()
        {
            return DbSet.ToList();
        }
    }
}
