﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Interfaces;

namespace Database.Repositories
{
    public class AirportsRepository : BaseRepository<Airport>, IAirportsRepository
    {
        public AirportsRepository(DatabaseContext context)
            : base(context)
        {  }

        public List<Airport> AiportCityNameSuggestions(string input)
        {
            return Context.Airports.Where(a => a.City.Contains(input)).ToList();
        }

        public Airport GetAirportByCode(string code)
        {
            //validate code
            return Context.Airports.Where(a => a.Code == code).SingleOrDefault();
        }
    }
}
