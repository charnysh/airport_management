﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBRepository
{
    public interface IRepository<T>
    {
        List<T> GetItems();
        T GetItem(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
