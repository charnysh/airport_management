﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;
using Database.Interfaces;

namespace Database.Repositories
{
    public class FlightsRepository : BaseRepository<Flight>, IFlightsRepository
    {
        public FlightsRepository(DatabaseContext context)
            : base(context)
        { }

        public List<Flight> FindFlightsByDepartureAirportID(int airportID)
        {
            return Context.Flights.Where(f => f.FromAirportID == airportID).ToList();
        }

        public List<Flight> FindFlightsByDestinationAirportID(int airportID)
        {
            return Context.Flights.Where(f => f.ToAirportID == airportID).ToList();
        }

        public List<Flight> GetFlightsByLeavingDate(DateTime dateTime)
        {
            return Context.Flights.Where(f => f.LeaveDate.Date == dateTime.Date).ToList();
        }

        public List<Flight> GetFlightsByArrivingDate(DateTime dateTime)
        {
            return Context.Flights.Where(f => f.ArriveDate.Date == dateTime.Date).ToList();
        }

    }
}
