﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Database.Interfaces;

namespace Database.Repositories
{
    public class SeatsRepository : BaseRepository<Seat>, ISeatsRepository
    {
        public SeatsRepository(DatabaseContext context)
            : base(context)
        { }

        public List<Seat> GetFlightReservedSeats(int flightId)
        {
            return Context.Reservations.Where(r => r.FlightId == flightId).Select(r => r.Seat).ToList();
        }

        public List<Seat> GetSeatsByFlightId(int flightId)
        {
            return Context.Flights.Where(f => f.ID == flightId).Select(f => f.Plane.Seats).Single();
        }

        public List<Seat> GetSeatsByPlaneId(int planeId)
        {
            return Context.Seats.Where(s => s.PlaneID == planeId).ToList();
        }
    }
}
