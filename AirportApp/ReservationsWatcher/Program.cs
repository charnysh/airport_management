﻿using System;
using System.Threading;
using Database;
using Microsoft.EntityFrameworkCore;

namespace ReservationsWatcher
{
    class Program
    {
        private static TimeSpan _expirationPeriod = TimeSpan.FromMinutes(15);
        private static string str = "Server=(localdb)\\MSSQLLocalDB;Database=AppDB;Trusted_Connection=True;MultipleActiveResultSets=true";
        static void Main(string[] args)
        {
            var timer = new Timer(
                callback: new TimerCallback(Run),
                state: null,
                dueTime: 1000,
                period: 1000
                );
            
        }

        static void Run(object timerState)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();

            var options = optionsBuilder
                    .UseSqlServer(str)
                    .Options;
            using (var dbContext = new DatabaseContext(options))
            {
                var cleaner = new ReservationsCleaner(dbContext, _expirationPeriod);
                int removed = cleaner.RemoveExpiredReservations(DateTime.Now);
                Console.WriteLine($"{removed} reservations removed");
            }
            return;
        }
    }
}
