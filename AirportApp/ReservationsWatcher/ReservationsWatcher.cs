﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database;
using Database.Interfaces;
using Models;

namespace ReservationsWatcher
{
    class ReservationsCleaner
    {
        TimeSpan ExpirationPeriod { get; } 
        DatabaseContext _context;
        public ReservationsCleaner(DatabaseContext context, TimeSpan expirationPeriod)
        {
            _context = context;
            ExpirationPeriod = expirationPeriod;
        }
        public int RemoveExpiredReservations(DateTime currentTime)
        {
            var expired = _context.Reservations.Where(r => currentTime - r.ReservationDate > ExpirationPeriod).ToList();
            _context.Reservations.RemoveRange(expired);
            _context.SaveChanges();
            return expired.Count;
        }

        
    }
}
