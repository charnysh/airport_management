﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;


namespace ReservationsWatcher
{
    public class AppDatabaseContext : DbContext
    {
        public DbSet<Reservation> Reservations { get; set; }

        public AppDatabaseContext(DbContextOptions<AppDatabaseContext> options)
                : base(options)
        {

        }
    }
}
