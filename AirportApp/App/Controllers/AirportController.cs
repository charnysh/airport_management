﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Models;
using Services.Interfaces;
using App.ViewModels;
using AutoMapper;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AirportController : BaseController<IAirportsManagementService, Airport, AirportViewModel>
    {
        public AirportController(IAirportsManagementService service, IMapper mapper) : base(service, mapper)
        { }

        [HttpGet("city/{input}")]
        public ActionResult<List<AirportViewModel>> AirportCityNameSuggestions(string input)
        {
            return Mapper.Map<List<AirportViewModel>>(Service.GetAiportCityNameSuggestions(input));
        }
    }
}