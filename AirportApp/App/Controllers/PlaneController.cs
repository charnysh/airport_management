﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using App.ViewModels;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaneController : BaseController<IPlanesManagementService, Plane, PlaneViewModel>
    {
        private IPlanesManagementService _planeManagementService;
        public PlaneController(IPlanesManagementService service, IMapper mapper) : base(service, mapper)
        {
            _planeManagementService = service;
        }

        [Authorize]
        [HttpGet("{id}/seats")]
        public ActionResult<List<Seat>> GetPlaneSeats(int id)
        {
            return _planeManagementService.GetPlaneSeats(id);
        }

        [Authorize]
        [HttpPut("{id}/seats")]
        public void AddSeats(int id, List<Seat> seats)
        {
            _planeManagementService.AddSeats(id, seats);
        }
    }
}