﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.ViewModels;
using Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "admin")]
    public class ReservationsController : Controller
    {
        private ISeatsManagementService _seatsManagementService;
        private IMapper _mapper;
        public ReservationsController(ISeatsManagementService seatsManagementService, IMapper mapper) : base()
        {
            _seatsManagementService = seatsManagementService;
            _mapper = mapper;
        }

        [HttpGet("reservations")]
        public ActionResult<List<ReservationViewModel>> GetAllReservations()
        {
            return _mapper.Map<List<ReservationViewModel>>(_seatsManagementService.GetReservations());
        }

        [HttpGet("tickets")]
        public ActionResult<List<TicketViewModel>> GetAllTickets()
        {
            var tickets = _seatsManagementService.GetTickets();
            return _mapper.Map<List<TicketViewModel>>(tickets);
        }
    }
}