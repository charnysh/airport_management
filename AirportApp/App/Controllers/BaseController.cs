﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services;
using AutoMapper;

using Utilities.ErrorHanding.Exceptions;
using Utilities.Validation;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public abstract class BaseController<TService, TModel,TViewModel > : Controller where TModel: class where TService: IManagementService<TModel>
    {
        protected TService Service;
        protected IMapper Mapper;
        public BaseController(TService repository, IMapper mapper)
        {
            this.Service = repository;
            this.Mapper = mapper;
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateModelState]
        public ActionResult<TViewModel> Create(TViewModel itemVM)
        {
            var item = Mapper.Map<TModel>(itemVM);
            Service.AddItem(item);
            return Mapper.Map<TViewModel>(item);
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public IActionResult RemoveItem(int id)
        {
            Service.RemoveItem(id);
            return NoContent();
        }

        [HttpGet("{id}")]
        public ActionResult<TViewModel> GetItem(int id)
        {
            var item = Service.GetItem(id);
            if (item == null)
            {
                throw new IdNotFoundException(id.ToString());
            }
            var itemVM = Mapper.Map<TViewModel>(item);
            return itemVM;
        }

        [HttpGet]
        public ActionResult<List<TViewModel>> GetItems()
        {
            return Mapper.Map<List<TViewModel>>(Service.GetItems());
        }
    }
}