﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Models;
using App.ViewModels;
using Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : BaseController<IFlightsManagementService, Flight, FlightViewModel>
    {

        public FlightController(IFlightsManagementService service, IMapper mapper) : base(service, mapper)
        {  }

        [Authorize]
        [HttpGet("departure/{id}")]
        public ActionResult<List<FlightViewModel>> FindFlightsByDepartureAirportID(int id)
        {
            return Mapper.Map<List<FlightViewModel>>(Service.FindFlightsByDepartureAirportID(id));
        }

        [Authorize]
        [HttpGet("destination/{id}")]
        public ActionResult<List<FlightViewModel>> FindFlightsByDestinationAirportID(int id)
        {
            return Mapper.Map<List<FlightViewModel>>(Service.FindFlightsByDestinationAirportID(id));
        }
    }
}