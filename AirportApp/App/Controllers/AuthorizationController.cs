﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Services.Interfaces;
using Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using App.ViewModels;
using App.Options;
using AutoMapper;
using Microsoft.Extensions.Options;
using Utilities.ErrorHanding.Exceptions;
using Utilities.Validation;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : Controller
    {
        private IUsersService _service;
        private IMapper _mapper;
        private IOptions<SecretOptions> _secretOptions;
        public AuthorizationController(IUsersService service, IMapper mapper, IOptions<SecretOptions> secretOptions)
        {
            this._service = service;
            this._mapper = mapper;
            this._secretOptions = secretOptions;//secretOptions.Value
        }

        [HttpPost("login")]
       
        public IActionResult LogIn(CredentialViewModel credentialVM)
        {            
            var credential = _mapper.Map<Credential>(credentialVM);
            return new ObjectResult(new { token = GenerateToken(credential, _secretOptions.Value.Secret) });
        }

        [HttpPost("signup")]
      
        public IActionResult SignUp(UserViewModel userVM)
        {
            var user = _mapper.Map<User>(userVM);
            _service.AddUser(user);

            return LogIn(userVM.Credential);
        }

        protected string GenerateToken(Credential recievedCredential,  string secret)
        {
            var credential = _service.GetCredentialByLogin(recievedCredential.Login);

            if (credential == null || !credential.Match(recievedCredential))
                throw new CredentialIncorrectException();

            var user = _service.GetUserByCredentialID(credential.ID);

            var claims = new Claim[] {
                new Claim(ClaimTypes.Name, credential.Login),
                new Claim(ClaimTypes.Role, user.Role),
            };

            var token = new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),
                    SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [Authorize (Roles = "admin")]
        [HttpGet]
        public ActionResult<List<User>> GetAllUsers()
        {
            return _service.GetUsers();
        }
    }
}
