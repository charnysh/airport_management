﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.ViewModels
{
    public class SeatViewModel
    {
        int ID { get; set; }

        public string Class { get; set; }

        public Seat.SeatPosition Position { get; set; }

        public int Number { get; set; }

        public int PlaneID { get; set; }

        public bool isReserved { get; set; }
    }
}
