﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace App.ViewModels
{
    public class AirportViewModel
    {
        public int ID { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        [StringLength(3)]
        public string Code { get; set; }
    }
}
