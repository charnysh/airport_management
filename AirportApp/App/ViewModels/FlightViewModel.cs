﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.ViewModels
{
    public class FlightViewModel
    {
        public int ID { get; set; }

        [Required]
        public int FromAirportID { get; set; }

        [Required]
        public int ToAirportID { get; set; }

        [Required]
        public DateTime LeaveDate { get; set; }

        [Required]
        public DateTime ArriveDate { get; set; }

        [Required]
        public int PlaneID { get; set; }

        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public Decimal ExcessBaggageCharge { get; set; }

        [Required]
        public int FreeBaggageWeight { get; set; }
    }
}
