﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.ViewModels
{
    public class UserViewModel
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Role { get; set; }
        public int CredentialID { get; set; }
        public CredentialViewModel Credential { get; set; }
    }
}
