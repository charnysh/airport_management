﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Models;
using Newtonsoft.Json;

namespace App.ViewModels
{
    public class PlaneViewModel
    {
        public int ID { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        [Range (1, int.MaxValue)]
        public int LoadCapacity { get; set; }

        
        public List<SeatViewModel> Seats { get; set; }
    }
}
