﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Options
{
    public class SecretOptions
    {
        public string Secret { get; set; }
        
    }
}
