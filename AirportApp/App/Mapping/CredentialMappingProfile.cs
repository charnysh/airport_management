﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
using Models.Interfaces;
using App.ViewModels;

namespace App.Mapping
{
    public class CredentialMappingProfile : Profile
    {
        public CredentialMappingProfile()
        {
            CreateMap<Credential, CredentialViewModel>();
            CreateMap<CredentialViewModel, Credential>();
        }     

    }
}
