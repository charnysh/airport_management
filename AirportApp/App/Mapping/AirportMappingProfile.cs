﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.ViewModels;
using AutoMapper;
using Models;

namespace App.Mapping
{
    public class AirportMappingProfile : Profile
    {
        public AirportMappingProfile()
        {
            CreateMap<Airport, AirportViewModel>();
            CreateMap<AirportViewModel, Airport>();
        }
    }
}
