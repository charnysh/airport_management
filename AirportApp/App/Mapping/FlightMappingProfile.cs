﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.ViewModels;
using AutoMapper;
using Models;

namespace App.Mapping
{
    public class FlightMappingProfile : Profile
    {
        public FlightMappingProfile()
        {
            CreateMap<Flight, FlightViewModel>();
            CreateMap<FlightViewModel, Flight>();
        }
    }
}
