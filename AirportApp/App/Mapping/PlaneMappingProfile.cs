﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.ViewModels;
using AutoMapper;
using Models;


namespace App.Mapping
{
    public class PlaneMappingProfile : Profile
    {
        public PlaneMappingProfile()
        {
            CreateMap<Plane, PlaneViewModel>();
            CreateMap<PlaneViewModel, Plane>();
        }
    }
}
