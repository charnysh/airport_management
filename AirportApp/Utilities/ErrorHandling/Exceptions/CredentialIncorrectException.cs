﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ErrorHanding.Exceptions
{
    public class CredentialIncorrectException : UnauthorizedAccessException
    {
        public override string ToString()
        {
            return "CredentialsIncorrectException";
        }
    }
}
