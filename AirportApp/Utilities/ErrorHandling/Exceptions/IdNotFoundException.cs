﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ErrorHanding.Exceptions
{
    public class IdNotFoundException: KeyNotFoundException
    {
        public IdNotFoundException(string message) : base(message)
        { }
    }
}
