﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApp.ViewModels;
using AutoMapper;
using Models;

namespace UserApp.Mapping
{
    public class FlightMappingProfile : Profile
    {
        public FlightMappingProfile()
        {
            CreateMap<Flight, FlightViewModel>();
            CreateMap<FlightViewModel, Flight>();
        }
    }
}
