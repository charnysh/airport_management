﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Models;
using UserApp.ViewModels;

namespace UserApp.Mapping
{
    public class TicketMappintProfile : Profile
    {
        public TicketMappintProfile()
        {
            CreateMap<Ticket, TicketViewModel>();
            CreateMap<TicketViewModel, Ticket>();
        }
    }
}
