﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using UserApp.ViewModels;
using Models;

namespace UserApp.Mapping
{
    public class SeatsMappingProfile : Profile
    {
        public SeatsMappingProfile()
        {
            CreateMap<Seat, SeatViewModel>().ForMember(
                dest => dest.isReserved,
                opt => opt.ResolveUsing(Resolver)
                );
        }

        private bool Resolver(Seat src, SeatViewModel dest, bool destMemeber, ResolutionContext context)
        {
            if (!context.Items.ContainsKey("flightId"))
            {
                if (src.Reservations.Count > 0 || src.Tickets.Count > 0)
                    return true;
                return false;
            }
            //check if it's int 
            if (src.Reservations.Any(r => r.FlightId == (int)context.Items["flightId"]))
                return true;

            if (src.Tickets.Any(t => t.FlightId == (int)context.Items["flightId"]))
                return true;
            return false;
        }
    }
}
