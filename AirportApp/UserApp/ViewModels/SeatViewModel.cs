﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace UserApp.ViewModels
{
    public class SeatViewModel
    {
        public int ID { get; set; }

        public string Class { get; set; }

        public Seat.SeatPosition Position { get; set; }

        public int Number { get; set; }

        public int PlaneID { get; set; }

        public bool isReserved { get; set; }
    }
}
