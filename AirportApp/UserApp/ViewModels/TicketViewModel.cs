﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApp.ViewModels
{
    public class TicketViewModel
    {
        public int ID { get; set; }

        public int UserId { get; set; }

        public SeatViewModel Seat { get; set; }

        public FlightViewModel Flight { get; set; }
    }
}
