﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace UserApp.ViewModels
{
    public class FlightViewModel
    {
        public int ID { get; set; }

        public int FromAirportID { get; set; }

        public int ToAirportID { get; set; }

        public DateTime LeaveDate { get; set; }

        public DateTime ArriveDate { get; set; }

        [Column(TypeName = "Money")]
        public Decimal ExcessBaggageCharge { get; set; }

        public int FreeBaggageWeight { get; set; }
    }
}
