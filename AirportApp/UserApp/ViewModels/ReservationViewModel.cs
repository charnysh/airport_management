﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApp.ViewModels
{
    public class ReservationViewModel
    {
        public int ID { get; set; }
        public int SeatId { get; set; }

        public int FlightId { get; set; }

        public int UserId { get; set; }

        public DateTime ReservationDate { get; set; }
    }
}
