﻿using Composition;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;
using UserApp.Options;
using Utilities.ErrorHanding;
using Database.Interfaces;
using Services;
using Services.Interfaces;
using Database.Repositories;

namespace UserApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddOptions();

            services.Configure<SecretOptions>(options =>
            {
                options.Secret = Configuration["Secret"];
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Jwt";
                options.DefaultChallengeScheme = "Jwt";
            }).AddJwtBearer("Jwt", options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,

                    ValidateIssuer = false,

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Secret"])),

                    ValidateLifetime = false
                };
            });

            CompositionRoot compositionRoot = new CompositionRoot(Configuration);
            compositionRoot.ComposeRoot(services);

            services.AddScoped<IReservationsRepository, ReservationsRepository>();

            services.AddScoped<ITicketsRepository, TicketsRepository>();

            services.AddScoped<ISeatsManagementService, SeatsManagementService>();
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseHsts();

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Airport}/{action=Index}/{id?}");
            });
        }
    }
}
