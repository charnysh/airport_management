﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserApp.ViewModels;
using Services.Interfaces;
using AutoMapper;
using Models;
using Microsoft.AspNetCore.Authorization;

namespace UserApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BookingController : ControllerBase
    {
        private ISeatsManagementService _seatsManagementService;
        private IMapper _mapper;
        public BookingController(ISeatsManagementService bookingService, IUsersService usersService, IMapper mapper)
        {
            _seatsManagementService = bookingService;
            _mapper = mapper;
        }

        [HttpPost]
        public void CreateReservation(ReservationViewModel reservation)
        {
            _seatsManagementService.CreateReservation(_mapper.Map<Reservation>(reservation), User.Identity.Name);
        }

        [HttpDelete("{id}")]
        public void CancelReservation(int id)
        {
            _seatsManagementService.CancelReservation(id);
        }

        [HttpPost("{id}")]
        public void ConfirmReservation(int id)
        {
            _seatsManagementService.CreateTicket(id);
        }

       
    }
}