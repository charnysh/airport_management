﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Services.Interfaces;
using UserApp.Options;
using UserApp.ViewModels;
using Models;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Utilities.ErrorHanding.Exceptions;

namespace UserApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private IUsersService _service;
        private IMapper _mapper;
        IOptions<SecretOptions> secretOptions;
        public AuthorizationController(IUsersService service, IMapper mapper, IOptions<SecretOptions> secretOptions)
        {
            _service = service;
            _mapper = mapper;
            this.secretOptions = secretOptions;
        }

        [HttpPost("login")]     
        public IActionResult LogIn(CredentialViewModel credentialVM)
        {
            var credential = _mapper.Map<Credential>(credentialVM);
            return new ObjectResult(new { token = GenerateToken(credential, secretOptions.Value.Secret) });
        }

        [HttpPost("signup")]
        public IActionResult SignUp(UserViewModel userVM)
        {
            var user = _mapper.Map<User>(userVM);
            _service.AddUser(user);

            return LogIn(userVM.Credential);
        }

        protected string GenerateToken(Credential recievedCredential, string secret)
        {
            var credential = _service.GetCredentialByLogin(recievedCredential.Login);

            if (credential == null || !credential.Match(recievedCredential))
                throw new CredentialIncorrectException();

            var user = _service.GetUserByCredentialID(credential.ID);

            var claims = new Claim[] {
                new Claim(ClaimTypes.Name, credential.Login),
                new Claim(ClaimTypes.Role, user.Role),
            };

            var token = new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),
                    SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}