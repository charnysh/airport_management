﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using UserApp.ViewModels;


namespace UserApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FlightsController : ControllerBase
    {
        /* Возможность найти рейс по аэропорту / городу отправления и назначения на нужную дату,
         * возможность подобрать рейсы на период туда и обратно,
         * возможность указать количество требуемых билетов 1 или более*/
        private IFlightsManagementService _flightsManagementService;
        private IMapper _mapper;

        public FlightsController(IFlightsManagementService flightsManagementService, IMapper mapper)
        {
            _flightsManagementService = flightsManagementService;
            _mapper = mapper;
        }

        [HttpGet]
        public List<FlightViewModel> GetFlights()
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetItems());
        }

        [HttpGet("departure/code/{code}")]
        public List<FlightViewModel> GetFlightsByDepartureAirportCode(string code)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByDepartureAirportCode(code));
        }

        [HttpGet("departure/city/{city}")]
        public List<FlightViewModel> GetFlightsByDepartureAirportCity(string city)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByDepartureAirportCity(city));
        }

        [HttpGet("destination/code/{code}")]
        public List<FlightViewModel> GetFlightsByDestinationAirportCode(string code)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByDestinationAirportCode(code));
        }

        [HttpGet("destination/city/{city}")]
        public List<FlightViewModel> GetFlightsByDestinationAirportCity(string city)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByDestinationAirportCity(city));
        }

        [HttpGet("leaving/{date}")]
        public List<FlightViewModel> GetFlightsByLeavingDate(DateTime date)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByLeavingDate(date));
        }
        [HttpGet("arriving/{date}")]
        public List<FlightViewModel> GetFlightsByArrivingDate(DateTime date)
        {
            return _mapper.Map<List<FlightViewModel>>(_flightsManagementService.GetFlightsByArrivingDate(date));
        }

        [HttpGet("{id}/seats")]
        public ActionResult<List<SeatViewModel>> GetFlightSeats(int id)
        {
           var seats = _flightsManagementService.GetSeats(id);
            return _mapper.Map<List<Seat>, List<SeatViewModel>>(seats, opt => opt.Items["flightId"] = id);
        }
    }
}