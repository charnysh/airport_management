import {
    FLIGHTS_LOADED,
    FLIGHTS_LOADING,
} from '../actions/flightsActions';

const initialState = {
    all: [],
    current: null,
    loading: false,
};

export default function flightsReducer(state = initialState, action) {
    switch (action.type) {
    case FLIGHTS_LOADED:
        return {
            all: action.payload,
            current: state.current,
            loading: false,
        }
    case FLIGHTS_LOADING:
        return {
            all: [],
            current: null,
            loading: true,
        };
    default:
        return Object.create(initialState);
    }
}
