import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import alertReducer from './alertReducer';
import statusReducer from './statusReducer';
import airportsReducer from './airportsReducer';
import flightsReducer from './flightsReducer';

const rootReducer = combineReducers({
    login: loginReducer,
    alert: alertReducer,
    status: statusReducer,
    airports: airportsReducer,
    flights: flightsReducer,
});

export default rootReducer;
