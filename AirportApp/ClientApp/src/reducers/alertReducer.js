import { ALERT_SUCCESS, ALERT_ERROR, ALERT_CLEAR } from '../actions/alertActions';


const initalState = {
    alertType: 'clear',
    message: '',
};

export default function alertReducer(state = initalState, action) {
    switch (action.type) {
    case ALERT_SUCCESS:
        return {
            alertType: 'success',
            message: action.message,
        };
    case ALERT_ERROR:
        return {
            alertType: 'danger',
            message: action.message,
        };
    case ALERT_CLEAR:
        return initalState;
    default:
        return state;
    }
}
