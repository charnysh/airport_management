import {
    AIRPORTS_LOADED,
    AIRPORT_LOADED,
    AIRPORTS_LOADING,
    AIRPORT_LOADING,
} from '../actions/airportActions';

const initialState = {
    all: [],
    current: null,
    loading: false,
};

export default function airportsReducer(state = initialState, action) {
    switch (action.type) {
    case AIRPORTS_LOADED:
        return {
            all: action.payload,
            current: state.current,
            loading: false,
        };
    case AIRPORT_LOADED:
        return {
            all: state.all,
            current: action.payload,
            loading: false,
        };
    case AIRPORTS_LOADING:
    case AIRPORT_LOADING:
        return {
            all: [],
            current: null,
            loading: true,
        };
    default:
        return Object.create(initialState);
    }
}
