import { LOADING } from '../actions/statusActions';

const initialState = {
    loading: false,
};

export default function statusReducer(state, action) {
    switch (action.type) {
    case LOADING:
        return {
            loading: true,
        };
    default:
        return initialState;
    }
}
