import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import FlightsTable from '../components/FlightsTable';
import FlightTableRow from '../components/FlightTableRow';
import FlightPageContainer from './FlightPageContainer';

import Spinner from '../components/Spinner';

import { loadFlightsAction } from '../actions/flightsActions';

class FlightsPageContainer extends Component {
    componentDidMount() {
        this.props.getFlights(this.props.token);
    }

    render() {
        let content = null;
        if (this.props.loading) {
            content = <Spinner />;
        } else {
            const rows = this.props.flights.all.map(f => <FlightTableRow key={f.id} flight={f} />);
            content = <FlightsTable rows={rows} />;
        }
        return (
            <div>
                <Switch>
                    <Route exact path="/flights" render={() => content} />
                    <Route path="/flights/:id" component={FlightPageContainer} />
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = store => ({
    flights: store.flights,
    loading: store.flights.loading,
    token: store.login.token,
});

const mapDispatchToProps = dispatch => ({
    getFlights: token => dispatch(loadFlightsAction(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FlightsPageContainer);