import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logoutAction } from '../actions/authorizationActions';
import Header from '../components/Header';

class HeaderContainer extends Component {
    render() {
        return (
            <Header userName={this.props.username} logoutAction={this.props.logoutAction} />
        );
    }
}

const mapStateToProps = store => ({
    username: store.login.user,
});

const mapDispatchToProps = dispatch => ({
    logoutAction: () => dispatch(logoutAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
