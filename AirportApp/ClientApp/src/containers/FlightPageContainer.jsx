import React, { Component } from 'react';
import { connect } from 'react-redux';

import FlightPage from '../components/FlightPage';
import Spinner from '../components/Spinner';

import { loadFlightAction } from '../actions/flightsActions';

class FlightPageContainer extends Component {
    componentDidMount() {
        this.props.getFlight(this.props.token, this.props.match.params.id);
    }

    render() {
        let content = null;
        if (this.props.loading) {
            content = <Spinner />;
        } else {
            content = <FlightPage flight={this.props.flight} />;
        }
        return (
            <div>
                { content }
            </div>
        );
    }
}

const mapStateToProps = store => ({
    flight: store.flights.current,
    loading: store.flights.loading,
    token: store.login.token,
});

const mapDispatchToProps = dispatch => ({
    getFlight: (token, id) => dispatch(loadFlightAction(token, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FlightPageContainer);
