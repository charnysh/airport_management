import React, { Component } from 'react';
import { connect } from 'react-redux';

import AirportsTable from '../components/AirportsTable';
import AirportTableRow from '../components/AirportTableRow';
import { loadAirportsAction } from '../actions/airportActions';
import Spinner from '../components/Spinner';

class AirportsTableContainer extends Component {
    componentDidMount() {
        this.props.getAirports(this.props.token);
    }

    render() {
        let content = null;
        if (this.props.loading) {
            content = <Spinner />;
        } else {
            const rows = this.props.airports.all.map(a => <AirportTableRow key={a.id} airport={a} />);
            content = (
                <AirportsTable rows={rows} />
            );
        }
        return (
            <div>
                { content }
            </div>
        );
    }
}

const mapStateToProps = store => ({
    airports: store.airports,
    loading: store.airports.loading,
    token: store.login.token,
});

const mapDispatchToProps = dispatch => ({
    getAirportAction: (token, id) => dispatch(getAirportAction(token, id)),
    getAirports: token => dispatch(loadAirportsAction(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AirportsTableContainer);
