import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';

class AlertContainer extends Component {
    render() {
        return (
            <Alert bsStyle={this.props.alert.alertType}>
                {this.props.alert.message}
            </Alert>
        );
    }
}

const mapStateToProps = store => ({
    alert: store.alert,
});

export default connect(mapStateToProps)(AlertContainer);
