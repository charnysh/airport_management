const LOADING = 'LOADING_STATUS';
const LOADED = 'LOADED';

export {
    LOADING,
    LOADED,
};

export const loadingAction = () => ({
    type: LOADING,
});

export const loadedAction = () => ({
    type: LOADED,
});
