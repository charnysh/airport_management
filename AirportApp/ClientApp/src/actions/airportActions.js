import { alertErrorAction } from './alertActions';
import { getAirports } from '../services/airportsService';

const AIRPORTS_LOADING = 'AIRPORTS_LOADING';
const AIRPORT_LOADING = 'AIPORT_LOADING';
const AIRPORTS_LOADED = 'AIRPORTS_LOADED';
const AIRPORT_LOADED = 'AIRPORT_LOADED';

export {
    AIRPORT_LOADED,
    AIRPORTS_LOADED,
    AIRPORTS_LOADING,
    AIRPORT_LOADING,
};

export function loadAirportsAction(token) {
    const airportsLoaded = airports => ({
        type: AIRPORTS_LOADED,
        payload: airports,
    });
    const airportsLoading = () => ({
        type: AIRPORTS_LOADING,
    });
    return (dispatch) => {
        dispatch(airportsLoading());
        getAirports(token)
            .then((responce) => {
                dispatch(airportsLoaded(responce));
            },
            (reject) => {
                dispatch(alertErrorAction(reject.message));
            });
    };
}
