import login from '../services/authorizationService';
import { loadingAction, loadedAction } from './statusActions';
import { alertErrorAction } from './alertActions';

const USER_LOGGINGIN = 'USER_LOGGININ';
const USER_LOGGEDIN = 'USER_LOOGEDIN';
const USER_LOGIN_FAILED = 'USER_LOGIN_REJECTED';
const USER_LOGGEDOUT = 'USER_LOGGEDOUT';

export {
    USER_LOGGINGIN,
    USER_LOGGEDIN,
    USER_LOGIN_FAILED,
    USER_LOGGEDOUT,
};

export function loginAction(username, password) {
    console.log(`loginActions.login ! ${username} p: ${password}`);

    const userLoggingIn = function (userName) {
        return { type: USER_LOGGINGIN, payload: userName };
    };
    const userLoggedIn = function (user) {
        return { type: USER_LOGGEDIN, payload: { user: user.username, token: user.token } };
    };
    const userLogInFailed = function (error) {
        return { type: USER_LOGIN_FAILED, payload: error };
    };

    return (dispatch) => {
        console.log('loginActions.login before  dispatch(userLoggingIn(username));:');
        dispatch(userLoggingIn(username));
        login(username, password)
            .then((responce) => {
                console.log('loginAction responce: ', responce);
                dispatch(userLoggedIn({
                    username,
                    token: responce.token,
                }));
            },
            (reject) => {
                console.log('loginAction reject: ', reject);
                dispatch(userLogInFailed(reject));
                if (reject.message) {
                    dispatch(alertErrorAction(reject.message));
                } else {
                    dispatch(alertErrorAction('Server error'));
                }
            });

        console.log('loginActions.login before dispatch(userLoggedIn():');
    };
}

export function logoutAction() {
    return { type: USER_LOGGEDOUT, payload: null };
}
