import { getFlights, getFlight } from '../services/flightsService';
import { alertErrorAction } from './alertActions';

const FLIGHTS_LOADING = 'FLIGHTS_LOADING';
const FLIGHTS_LOADED = 'FLIGHTS_LOADED';
const FLIGHT_LOADING = 'FLIGHT_LOADING';
const FLIGHT_LOADED = 'FLIGHT_LOADED';

export {
    FLIGHTS_LOADED,
    FLIGHTS_LOADING,
    FLIGHT_LOADING,
    FLIGHT_LOADED,
};

export function loadFlightsAction(token) {
    console.log("loadFlightsAction");
    const flightsLoading = () => ({
        type: FLIGHTS_LOADING,
    });
    const flightsLoaded = flights => ({
        type: FLIGHTS_LOADED,
        payload: flights,
    });
    return (dispatch) => {
        dispatch(flightsLoading());
        getFlights(token)
            .then(
                response => dispatch(flightsLoaded(response)),
                reject => dispatch(alertErrorAction(reject.message)),
            );
    };
}

export function loadFlightAction(token, id) {
    const flightLoading = () => ({
        type: FLIGHT_LOADING,
    });
    
    const flightLoaded = flight => ({
        type: FLIGHT_LOADED,
        payload: flight,
    });

    return (dispatch) => {
        dispatch(flightLoading());
        getFlight(token, id)
            .then(
                response => dispatch(flightLoaded(response)),
                reject => dispatch(alertErrorAction(reject.message)),
            );
    };
}
