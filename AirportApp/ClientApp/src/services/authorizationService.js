import { API_URL } from './constants/constants';
import { handleError, handleResponce } from './responceHandlers';

export default function (login, password) {
    const requestOptions = {
        method: 'Post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ login, password }),
    };
    return fetch(`${API_URL}/authorization/login`, requestOptions)
        .then(handleResponce, handleError);
}
