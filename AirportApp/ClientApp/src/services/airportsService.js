import { API_URL } from './constants/constants';
import { handleError, handleResponce } from './responceHandlers';

const getGetRequestOptions = token => ({
    method: 'Get',
    headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    },
});

function getAirports(token) {
    return fetch(`${API_URL}/airport/`, getGetRequestOptions(token))
        .then(handleResponce, handleError);
}

function getAirportById(id) {

}

export {
    getAirports,
    getAirportById,
};
