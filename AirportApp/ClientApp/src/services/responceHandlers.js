export function handleResponce(responce) {
    return new Promise((resolve, reject) => {
        console.log(responce);
        if (responce.ok) {
            responce.json().then(json => resolve(json));
        } else {
            responce.json().then(json => reject(json));
        }
    });
}

export function handleError(error) {
    console.log("err:", error);
    return Promise.reject(error);
}
