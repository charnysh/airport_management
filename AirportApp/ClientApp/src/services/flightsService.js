import { API_URL } from './constants/constants';
import { handleError, handleResponce } from './responceHandlers';

const getGetRequestOptions = token => ({
    method: 'Get',
    headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    },
});

function getFlights(token) {
    return fetch(`${API_URL}/flight/`, getGetRequestOptions(token))
        .then(handleResponce, handleError);
}

function getFlight(token, id) {
    return fetch(`${API_URL}/flight/${id}`, getGetRequestOptions(token))
        .then(handleResponce, handleError);
}

export {
    getFlights,
    getFlight,
};
