import React from 'react';
import PropTypes from 'prop-types';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const FlightsPage = props => (
    <div>
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Departure</TableCell>
                        <TableCell>Destination</TableCell>
                        <TableCell>Departure Time</TableCell>
                        <TableCell>Arriving Time</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.rows}
                </TableBody>
            </Table>
        </Paper>
    </div>
);

FlightsPage.propTypes = {
    rows: PropTypes.array.isRequired,
};

export default FlightsPage;
