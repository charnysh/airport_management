import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import Logout from './Logout';

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class Header extends Component {
    render() {
        return (
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="title" color="inherit" className={this.props.classes.flex}>
                        Airport Management App
                    </Typography>
                    <Logout logoutAction={this.props.logoutAction} userName={this.props.userName} />
                </Toolbar>
            </AppBar>
                
        );
    }
}


Header.propTypes = {
    userName: PropTypes.string,
    logoutAction: PropTypes.func.isRequired,
};

export default withStyles(styles)(Header);
