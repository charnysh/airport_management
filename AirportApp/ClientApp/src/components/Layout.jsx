import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import AirportsPageContainer from '../containers/AirportsTableContainer';
import FlightsPageContainer from '../containers/FlightsPageContainer';
import LeftNavigation from './LeftNavigation';
import LoginContainer from '../containers/LoginContainer';
import HeaderContainer from '../containers/HeaderContainer';
import AlertContainer from '../containers/AlertContainer';

import AirportPage from './AirportPage';

import '../styles/styles.less';

const Layout = props => (
    <BrowserRouter>
        <div>
            <HeaderContainer />
            <AlertContainer />
            {props.authorized && (
                <main className="main">
                    <LeftNavigation />
                    <Switch>
                        <Route exact path="/" component={AirportsPageContainer} />
                        <Route path="/airports/:id" component={AirportPage} />
                        <Route path="/flights" component={FlightsPageContainer} />
                    </Switch>
                </main>
            )}
            {!props.authorized && (
                <LoginContainer />
            )}
        </div>
    </BrowserRouter>
);

const mapStateToProps = store => ({
    authorized: store.login.loggedIn,
});

export default connect(mapStateToProps)(Layout);
