import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

export default class Logout extends Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        this.props.logoutAction();
    }

    render() {
        const { userName } = this.props;
        return (
            <div>
                { userName && (
                    <div className="logout">
                        <Tooltip title={userName}>
                            <Button type="button" onClick={this.handleLogout}>Log Out</Button>
                        </Tooltip>
                    </div>
                )}
            </div>
        );
    }
}

Logout.propTypes = {
    userName: PropTypes.string,
    logoutAction: PropTypes.func.isRequired,
};
