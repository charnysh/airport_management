import React from 'react';
import PropTypes from 'prop-types';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const AirportsTable = props => (
    <Paper>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Airport City</TableCell>
                    <TableCell>Airport Code</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {props.rows}
            </TableBody>
        </Table>
    </Paper>
);

AirportsTable.propTypes = {
    rows: PropTypes.array.isRequired,
};

export default AirportsTable;
