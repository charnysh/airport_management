import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import '../styles/styles.less';

const styles = theme => ({
    leftNav: {
        backgroundColor: theme.palette.background.paper,
    },
});

const options = [
    'Add New Airport',
    'Add New Plane Type',
    'Add New Flight',
];

class LeftNavigation extends Component {
    button = null;

    state = {
        anchorEl: null,
    };

    componentDidMount() {

    }

    handleClickListItem = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuItemClick = (event, index) => {
        console.log(index);
        this.setState({ anchorEl: null });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { anchorEl } = this.state;

        return (
            <div className="leftNav">
                <List component="nav">
                    <ListItem
                        button
                        aria-haspopup="true"
                        aria-controls="lock-menu"
                        aria-label="Add New"
                        onClick={this.handleClickListItem}
                    >
                        <ListItemText primary="Add new" />
                    </ListItem>
                    <ListItem
                        button
                        aria-label="Airports"

                    >
                        <Link to="/">Airports</Link>
                    </ListItem>
                    <ListItem
                        button
                        aria-label="Plane Types"

                    >
                        <ListItemText primary="Plane Types" />
                    </ListItem>
                    <ListItem
                        button
                        aria-label="Flights"
                    >
                        <Link to="/flights">Flights</Link>
                    </ListItem>
                </List>
                <Menu
                    id="add-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    {options.map((option, index) => (
                        <MenuItem
                            key={option}
                            selected={index === this.state.selectedIndex}
                            onClick={event => this.handleMenuItemClick(event, index)}
                        >
                            {option}
                        </MenuItem>
                    ))}
                </Menu>
            </div>
        );
    }
}

LeftNavigation.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LeftNavigation);
