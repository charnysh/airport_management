import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

class FlightTableRow extends Component {
    render() {
        const { flight } = this.props;
        return (
            <TableRow>
                <TableCell>{flight.fromAirportID}</TableCell>
                <TableCell>{flight.toAirportID}</TableCell>
                <TableCell>{flight.leaveDate}</TableCell>
                <TableCell>{flight.arriveDate}</TableCell>
                <TableCell><Link to={`/flights/${flight.id}`}>more</Link></TableCell>
            </TableRow>
        );
    }
}

FlightTableRow.propTypes = {
    flight: PropTypes.object.isRequired,
};

export default FlightTableRow;
