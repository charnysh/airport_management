import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

class AirportTableRow extends Component {
    render() {
        const { airport, getAirportAction } = this.props;
        return (
            <TableRow onClick={getAirportAction}>
                <TableCell>{airport.city}</TableCell>
                <TableCell>{airport.code}</TableCell>
                <TableCell><Link to={`/airports/${airport.id}`}>{airport.id}</Link></TableCell>
            </TableRow>
        );
    }
}

AirportTableRow.propTypes = {
    airport: PropTypes.object.isRequired,
    getAirportAction: PropTypes.func.isRequired,
};

export default AirportTableRow;
