import React from 'react';
import PropTypes from 'prop-types';

const FlightPage = props => (
    <div>
        <h1>Flight #{props.flight.id}</h1>
    </div>
);

FlightPage.propTypes = {
    flight: PropTypes.object.isRequired,
};

export default FlightPage;
